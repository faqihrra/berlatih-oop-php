<?php
require_once("animal.php");
/**
 * 
 */
class Frog extends Animal
{
	public $jump = "Hop Hop";
	public $legs = 4;
	public $cold_blooded = "True";
	function __construct($name)
	{
		$this->name = $name;
	}
	function jump(){
		echo $this->jump;
	}
}


?>