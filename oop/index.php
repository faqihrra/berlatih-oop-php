<?php
require_once("animal.php");
require("ape.php");
require("frog.php");

//Release 0
$sheep = new Animal("shaun");

echo "Nama Hewan   : $sheep->name <br>"; // "shaun"
echo "Jumlah Kaki  : $sheep->legs <br>"; // 2
echo "Berdarah dingin : $sheep->cold_blooded <br>"; // false


echo "<br>";
echo "<br>";
echo "<br>";

//Release 1

$sungokong = new Ape("kera sakti");
echo "Nama Hewan  : $sungokong->name <br>";
echo "Suara Hewan  : $sungokong->yell <br>";// "Auooo"
echo "Jumlah Kaki  : $sungokong->legs <br>";
echo "Berdarah Dingin : $sungokong->cold_blooded <br>";
echo "<br>";
$kodok = new Frog("buduk");
echo "Nama Hewan  : $kodok->name <br>";
echo "Suara Melompat  : $kodok->jump <br>"; // "hop hop"
echo "Jumlah kaki   : $kodok->legs <br>";
echo "Berdarah Dingin : $kodok->cold_blooded <br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>